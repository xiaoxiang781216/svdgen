# svdgen

This tool generates register macro definitions for NuttX based on CMSIS-SVD files. This avoids manual translation from datasheet into code.

# Install

This script is based on python3. You need to install dependencies with the following command:

    pip3 install cmsis-svd

This tool is based on the SVD database bundled in cmsis-svd python package, found at: https://github.com/posborne/cmsis-svd
(check `data` directory).

# Run

You can run the tool as follows:

   ./gen.py -v Nordic -d nrf51 -p RADIO -x NRF51 

The first parameter (vendor) corresponds to a subdirectory under `data` of the CMSIS-SVD repo, whereas the second parameter (device) corresponds to the svd filename (without '.svd' extension). The third parameter is the peripheral name and the final parameter is the prefix to add to all macro definitions. The result is printed in the console so that you can paste this into the apropriate header file.

If you pass '?' as peripheral name, you will see a list of all defined peripherals in the SVD file. Moreover, if you pass "map" as peripheral name, you will generate definitions of base addresses of all peripherals. 
